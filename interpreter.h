#ifndef INTERPRETER_H
#define INTERPRETER_H
#include "gestor.h"
#include "figurasFactory.h"
class Interpreter
{ //Gestiona las figuras

private:
    CirculoFactory creadorCirculos;             // Fabricas
    TrianguloFactory creadorTriangulos;
    CuadradoFactory creadorCuadrados;
    Gestor gestor;
    void crearCirculo();                        //Creadores de figuras que llaman a las fabricas
    void crearRect();
    void crearTriangulo();

    void trasladarX(int index);                 //metodos para trasalada figura
    void trasladarY(int index);
    void trasladar();
    void eliminar();                            //borrar figuras
    void mostrar();                             //mostrar figuras

public:
    Interpreter();
    int menu();
};

#endif
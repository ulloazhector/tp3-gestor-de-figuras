#include "figurasFactory.h"
#include <iostream>
using namespace std;

CirculoFactory:: CirculoFactory(){}

Figura* CirculoFactory::crearFigura()
{
    Circulo *circulo;               //puntero a el circulo que se esta por crear
    Coordenada centro;              
    int radio;
    cout << "Ingrese coordenadas del centro (x,y): ";   //leemos los datos para crear el circulo
    cin >> centro.x;
    cin >> centro.y;
    cout << "Ingrese el radio R: ";
    cin >> radio;
    circulo = new Circulo(centro, radio);       //creamos el circulo
    return circulo;                             //retornamos la figura recien creada
}
TrianguloFactory::TrianguloFactory(/* args */){}

Figura* TrianguloFactory::crearFigura()
{
    Coordenada c[3];
    Triangulo *triangulo;
    for (size_t i = 0; i < 3; i++)
    {
        cout << "Ingrese el vertice " << i + 1 << " del triangulo: ";
        cin >> c[i].x;
        cin >> c[i].y;
    }

    triangulo = new Triangulo(c);
    return triangulo;
}

CuadradoFactory::CuadradoFactory(/* args */){}

Figura* CuadradoFactory::crearFigura()
{
    Coordenada c[4];
    Cuadrado *cuadrado;
    for (size_t i = 0; i < 4; i++)
    {
        cout << "Ingrese el vertice " << i + 1 << " del cuadrilatero: ";
        cin >> c[i].x;
        cin >> c[i].y;
    }

    cuadrado = new Cuadrado(c);
   return cuadrado;
}
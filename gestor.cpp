#include "gestor.h"
#include <iostream>

using namespace std;

Gestor :: Gestor(){
    return;
}

void Gestor :: agregarFigura(Figura *f){
    figuras.push_back(f);
    return;
}

void Gestor :: eliminarFigura(int index){//toma valores a partir de 1
    list<Figura*>::iterator it;
    if((index > 0) && (figuras.size() >= index)){
        it = figuras.begin(); //Iterador it apunta al inicio de la lista
        advance(it,index-1);  //Movemos el iterador a la posicion a eliminar
        delete *it;
        figuras.erase(it);
    }else{
        cout<< "Error: Figura inexistente." << endl << endl;
    }
    return;
}

void Gestor :: mostrar(){
    if(!figuras.empty()){
        int i = 0;
        list<Figura*>::iterator it;
        for (it = figuras.begin(); it!=figuras.end(); ++it){
            cout << "Figura: " << ++i << endl;
            (*it)->mostrarse();
            cout << "***************" << endl;
        }
        cout << endl;
    }
}

void Gestor :: trasladarX(int x, int index){
    list<Figura*>::iterator it;
    if((index > 0) && (figuras.size() >= index)){
        it = figuras.begin();
        advance(it, index-1);
        (*it)->trasladarX(x);
    }else{
        cout<< "Error: Figura inexistente." << endl << endl;
    }
    return;
}

void Gestor :: trasladarY(int y, int index){
    list<Figura*>::iterator it;
    if((index > 0) && (figuras.size() >= index)){
        it = figuras.begin();
        advance(it, index-1);
        (*it)->trasladarY(y);
    }else{
        cout<< "Error: Figura inexistente." << endl << endl;
    }
    return;
}


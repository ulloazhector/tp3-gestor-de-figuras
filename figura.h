#ifndef FIGURA_H
#define FIGURA_H
#include "coordenadas.h"

class Figura
{
public:
    virtual void mostrarse() = 0;
    virtual void trasladarX(int x) = 0;
    virtual void trasladarY(int y) = 0;
};

class Triangulo : public Figura
{
private:
    Coordenada c[3];

public:
    Triangulo(Coordenada coordenadas[3]);
    void mostrarse();
    void trasladarX(int x);
    void trasladarY(int y);
};

class Circulo : public Figura
{
private:
    Coordenada centro;
    int radio;

public:
    Circulo(Coordenada centro, int radio); //constructor
    void mostrarse();
    void trasladarX(int x);
    void trasladarY(int y);
};

class Cuadrado : public Figura
{
private:
    Coordenada c[4];

public:
    Cuadrado(Coordenada[4]); //constructor
    void mostrarse();
    void trasladarX(int x);
    void trasladarY(int y);
};

#endif
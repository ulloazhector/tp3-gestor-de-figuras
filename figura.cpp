#include "figura.h"
#include <iostream>

using namespace  std;

Circulo :: Circulo(Coordenada centro, int radio)
{
    this->centro = centro;
    this->radio = radio;
    return;
}

void Circulo :: mostrarse()
{
    cout << "Centro: (" << centro.x << "," << centro.y << ")" << endl;
    cout << "Radio:" << radio << endl;
    return;
}

void Circulo :: trasladarX(int x)
{
    this->centro.x += x;
    return;
}

void Circulo :: trasladarY(int y)
{
    this->centro.y += y;
    return;
}

/*
Metodos de Cuadrado 
*/

Cuadrado :: Cuadrado(Coordenada c[]){
    for(int i = 0; i < 4; i++){
        this->c[i].x = c[i].x;
        this->c[i].y = c[i].y;
    }
    return;
}

void Cuadrado :: mostrarse(){
    cout << "Coordenadas: " << endl;
    for(int i = 0; i < 4; i++)
        cout << "(" << c[i].x << ", " << c[i].y << ")" << endl;
}

void Cuadrado :: trasladarX(int x){
    for(int i = 0; i < 4; i++)
        c[i].x += x;
}

void Cuadrado :: trasladarY(int y){
    for(int i = 0; i < 4; i++)
        c[i].y += y;
}

/*
Metodos de Triangulo
*/

Triangulo :: Triangulo(Coordenada coordenadas[3])
{
    for(int i =0; i <3; i++)
    {
        this->c[i]= coordenadas[i];
    }
}

void Triangulo :: mostrarse()
{
    cout << "Coordenadas: " << endl;
        for(int i = 0; i < 3; i++)
            cout << "(" << c[i].x << ", " << c[i].y << ")" << endl;
}

void Triangulo :: trasladarX(int x)
{
    this->c[0].x += x;
    this->c[1].x += x;
    this->c[2].x += x;
}

void Triangulo :: trasladarY(int y)
{
    this->c[0].y += y;
    this->c[1].y += y;
    this->c[2].y += y;
}
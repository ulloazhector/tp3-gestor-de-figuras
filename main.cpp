#include <iostream>
#include "interpreter.h"

using namespace std;

int main(int argc, char const *argv[])
{
    Interpreter interpreter;
    while (interpreter.menu());
    return 0;
}

#ifndef GESTOR_H
#define GESTOR_H

#include <iostream>
#include <list>
#include "figura.h"

using namespace std;


class Gestor{                               //Gestiona las figuras
    private:
        list<Figura*> figuras;
        //Figura *figuras[5];
    public:
        Gestor();
        void agregarFigura(Figura *f);
        void eliminarFigura(int index);
        void mostrar();
        void trasladarX(int x, int index);      //Mueve una unica figura, indicada por index.
        void trasladarY(int y, int index);

};


#endif
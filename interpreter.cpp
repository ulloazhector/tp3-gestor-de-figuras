#include "interpreter.h"
#include <iostream>
#include "figurasFactory.h"
using namespace std;

Interpreter::Interpreter()
{
    return;
}

void Interpreter::crearCirculo()
{
    Figura *circulo;                            //puntero a una figura
    circulo = creadorCirculos.crearFigura();    // guardamos la direccion de la figura recien creada
    (this->gestor).agregarFigura(circulo);      // agregamos la figura
    return;
}

void Interpreter::crearRect()
{
    Figura *cuadrado;
    cuadrado=creadorCuadrados.crearFigura();
    (this->gestor).agregarFigura(cuadrado);
    return;
}
void Interpreter::crearTriangulo()
{
    Figura *triangulo;
    triangulo = creadorTriangulos.crearFigura();   
    (this->gestor).agregarFigura(triangulo);
    return;
}
void Interpreter::trasladar()
{
    int index;
    cout << "Ingrese la figura que quiere trasladar: ";
    cin >> index;
    trasladarX(index);
    trasladarY(index);
    return;
}
void Interpreter::trasladarX(int index)
{
    int x;
    cout << " Ingrese la cantidad que sea desplazar x: ";
    cin >> x;
    gestor.trasladarX(x,index);
    return;
}
void Interpreter::trasladarY(int index)
{
    int y;
    cout << " Ingrese la cantidad que sea desplazar y: ";
    cin >> y;
    gestor.trasladarY(y,index);
    return;
}
void Interpreter::mostrar()
{
    gestor.mostrar();
    return;
}
void Interpreter::eliminar(){
    int index;
    cout<<" Ingrese la posicion de la figura que quiere eliminar: ";
    cin>>index;
    gestor.eliminarFigura(index);
}

int Interpreter::menu(){
    
    int op;
    cout<<"Ingrese una opcion: "<<endl;
    cout<<"(1) Crear un circulo "<<endl;
    cout<<"(2) Crear un rectangulo "<<endl;
    cout<<"(3) Crear un triangulo "<<endl;
    cout<<"(4) Trasladar una figura "<<endl;
    cout<<"(5) Mostrar todas las figuras "<<endl;
    cout<<"(6) Eliminar una figura"<<endl;
    cout<<"(0) Salir "<<endl;
    cin>>op;
    switch (op)
    {
    case 1:
        crearCirculo();
        break;
    
    case 2:
        crearRect();
        break;
    
    case 3:
        crearTriangulo();
        break;
    
    case 4:
        trasladar();
        break;
    case 5:
        mostrar();
        break;
    case 6:
        eliminar();
        break;
    
    case 0:
        break;
    
    default:
        cout<<"Opcion invalida."<<endl;
        break;
    }
    return op;
}
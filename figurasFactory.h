#ifndef FIGURASFACTORY_H
#define FIGURASFACTORY_H
#include "figura.h"

class FigurasFactory                    //Interfase para crear figuras
{
private:
    /* data */
public:
    virtual Figura* crearFigura() = 0;
};


class CirculoFactory: public FigurasFactory
{
private:
    /* data */
public:
    CirculoFactory(/* args */);
    Figura* crearFigura();
};

class TrianguloFactory: public FigurasFactory
{
private:
    /* data */
public:
    TrianguloFactory(/* args */);
    Figura* crearFigura();
};

class CuadradoFactory: public FigurasFactory
{
private:
    /* data */
public:
    CuadradoFactory(/* args */);
    Figura* crearFigura();

};




#endif